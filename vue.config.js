module.exports = {
  devServer:{
    host:'0.0.0.0',
    port:8080,
    proxy:{
      '/home/*':{
       target:'http://127.0.0.1:8887',
        changeOrigin:true,
        // pathRewrite:{
        //   '/home':''
        // }
      },
        '/pms/*':{
            target:'http://127.0.0.1:8888',
            changeOrigin:true,
        }
        ,
        '/cart/*':{
            target:' http://127.0.0.1:8888',
            changeOrigin:true,
        }
        ,
        '/sso/*':{
            target:'http://127.0.0.1:8888',
            changeOrigin:true,
        }
        ,
        '/member/*':{
            target:'http://127.0.0.1:8888',
            changeOrigin:true,
        } ,
        '/order/*':{
            target:'http://127.0.0.1:8888',
            changeOrigin:true,
        },
        '/static/qrcode/*':{
          target:'http://127.0.0.1:8844',
          changeOrigin:true,
        },
        '/es/*':{
            target:'http://127.0.0.1:8054',
            changeOrigin:true,
            pathRewrite:{
              '/es':''
            }
        }
    }
  },
  // publicPath:'/app',
  // outputDir:'dist',
  // indexPath:'index2.html',
  // lintOnSave:false,
  productionSourceMap:true,
  chainWebpack:(config)=>{
    config.plugins.delete('prefetch');
  }
}